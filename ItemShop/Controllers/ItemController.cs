﻿using ItemShop.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace ItemShop.Controllers
{
    public class ItemController : Controller
    {
        private readonly ILogger<ItemController> logger;
        private readonly IItemService itemService;

        public ItemController(ILogger<ItemController> logger, IItemService itemService)
        {
            this.logger = logger;
            this.itemService = itemService;
        }

        [HttpPost]
        public async Task<IActionResult> ItemForSaleAsync(Guid playerId, Guid itemId, decimal price)
        {
            logger.LogInformation($"Player {playerId} is putting item {itemId} up for sale.");
            var itemSales = await itemService.PostItemForSale(playerId, itemId, price);

            if (itemSales == null)
                return BadRequest();
            else
                return Created(string.Empty, itemSales);
        }

        [HttpPost]
        public IActionResult ItemPurchase(Guid playerId, Guid itemSalesId)
        {
            logger.LogInformation($"Player {playerId} is purchasing {itemSalesId}.");
            var itemSales = itemService.PurchaseItem(playerId, itemSalesId);
            return Json(itemSales);
        }

        [HttpGet]
        public IActionResult ItemForSale(int numberOfPlayersToSearch)
        {
            if (numberOfPlayersToSearch < 1)
                return BadRequest();

            var itemSales = itemService.GetItemsForSale(numberOfPlayersToSearch);
            return Json(itemSales);
        }
    }
}
