﻿using ItemShop.Data;
using ItemShop.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace ItemShop.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class, ICreateUpdateEntity
    {
        private readonly ApplicationDbContext applicationDbContext;
        internal readonly DbSet<T> dbset;

        public GenericRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
            dbset = this.applicationDbContext.Set<T>();
        }

        public async Task<bool> Add(T entity)
        {
            entity.CreatedDateTime = DateTime.UtcNow;
            entity.UpdatedDateTime = DateTime.UtcNow;
            await dbset.AddAsync(entity);
            return true;
        }

        public async Task<T> GetById(Guid id)
        {
            var result = await dbset.FindAsync(id);
            return result;
        }

        public void Update(T entity)
        {
            entity.UpdatedDateTime = DateTime.UtcNow;
            dbset.Attach(entity);
            applicationDbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
