﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItemShop.Models
{
    public class ItemSale : ICreateUpdateEntity
    {
        [Key]
        public Guid ItemSaleId { get; set; }

        [ForeignKey("Item")]
        public Guid ItemId { get; set; }

        public Item Item { get; set; }

        [ForeignKey("Player")]
        public Guid SellingPlayerId { get; set; }

        public Player SellingPlayer
        {
            get => lazyLoader.Load(this, ref sellingPlayer);
            set => sellingPlayer = value;
        }

        [ForeignKey("Player")]
        public Guid? BuyingPlayerId { get; set; }

        public Player BuyingPlayer { get; set; }

        public decimal Price { get; set; }

        public bool Active { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime UpdatedDateTime { get; set; }

        public ItemSale() { }

        private ItemSale(ILazyLoader lazyLoader)
        {
            this.lazyLoader = lazyLoader;
        }

        private ILazyLoader lazyLoader;
        private Player sellingPlayer;
    }
}
