﻿using ItemShop.Models;
using System;

namespace ItemShop.Repository
{
    public interface IPlayerRepository : IGenericRepository<Player>
    {
    }
}
