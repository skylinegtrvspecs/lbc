﻿using System;
using System.Runtime.Serialization;

namespace ItemShop.Services
{
    [Serializable]
    internal class CannotCompletePurchase : Exception
    {
        public CannotCompletePurchase()
        {
        }

        public CannotCompletePurchase(string message) : base(message)
        {
        }

        public CannotCompletePurchase(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CannotCompletePurchase(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}