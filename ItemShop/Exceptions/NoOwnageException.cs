﻿using System;
using System.Runtime.Serialization;

namespace ItemShop.Services
{
    [Serializable]
    internal class NoOwnageException : Exception
    {
        public NoOwnageException()
        {
        }

        public NoOwnageException(string message) : base(message)
        {
        }

        public NoOwnageException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoOwnageException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}