﻿using ItemShop.Data;
using ItemShop.Models;
using System;
using System.Linq;

namespace ItemShop.Repository
{
    public class PlayerItemRepository : GenericRepository<PlayerItem>, IPlayerItemRepository
    {
        public PlayerItemRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }

        public PlayerItem Find(Guid playerId, Guid itemId) =>
            dbset.FirstOrDefault(playerItem => playerItem.PlayerId == playerId && playerItem.ItemId == itemId && playerItem.Active);
    }
}
