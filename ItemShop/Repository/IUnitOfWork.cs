﻿using System.Threading.Tasks;

namespace ItemShop.Repository
{
    public interface IUnitOfWork
    {
        IItemRepository Items { get; }
        IItemSaleRepository ItemSales { get; }
        IPlayerRepository Players { get; }
        IPlayerItemRepository PlayerItems { get; }

        Task CompleteAsync();
    }
}
