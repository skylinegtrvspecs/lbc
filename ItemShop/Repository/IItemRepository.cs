﻿using ItemShop.Models;

namespace ItemShop.Repository
{
    public interface IItemRepository : IGenericRepository<Item>
    {
    }
}