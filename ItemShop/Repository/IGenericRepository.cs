﻿using ItemShop.Models;
using System;
using System.Threading.Tasks;

namespace ItemShop.Repository
{
    public interface IGenericRepository<T> where T : ICreateUpdateEntity
    {
        Task<T> GetById(Guid id);
        Task<bool> Add(T entity);
        void Update(T entity);
    }
}
