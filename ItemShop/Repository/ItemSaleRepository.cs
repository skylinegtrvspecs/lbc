﻿using ItemShop.Data;
using ItemShop.Models;
using System;
using System.Linq;

namespace ItemShop.Repository
{
    public class ItemSaleRepository : GenericRepository<ItemSale>, IItemSaleRepository
    {
        public ItemSaleRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }

        public ItemSale Find(Guid playerId, Guid itemId) =>
            dbset
                .FirstOrDefault(itemSale => itemSale.SellingPlayerId == playerId && itemSale.ItemId == itemId && itemSale.Active);

        public ItemSale[] FindByPlayerIds(Guid[] playerIds) =>
            dbset
                .Where(itemSale => playerIds.Any(playerId => playerId == itemSale.SellingPlayerId) && itemSale.Active)
            .ToArray();

        public Guid[] GetRandom(int numberOfPlayersToSearch)
        {
            var count = dbset.Where(itemSale=>itemSale.Active)
                .GroupBy(itemSale => itemSale.SellingPlayerId)
                .Count() / numberOfPlayersToSearch;
            var index = new Random().Next(count);
            var result = new Guid[numberOfPlayersToSearch];

            for (var i = 1; i <= numberOfPlayersToSearch; i++)
            {
                result[i - 1] = dbset.Where(itemSale => itemSale.Active)
                    .Skip(i * index)
                    .FirstOrDefault()
                    .SellingPlayerId;
            }

            return result;
        }
    }
}
