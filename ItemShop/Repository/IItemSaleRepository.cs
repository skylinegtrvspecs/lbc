﻿using ItemShop.Models;
using System;

namespace ItemShop.Repository
{
    public interface IItemSaleRepository : IGenericRepository<ItemSale>
    {
        ItemSale[] FindByPlayerIds(Guid[] playerIds);
        ItemSale Find(Guid playerId, Guid itemId);
        Guid[] GetRandom(int numberOfPlayersToSearch);
    }
}
