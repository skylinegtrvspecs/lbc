﻿using ItemShop.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace ItemShop.Data
{
    public class ApplicationDbContext : DbContext
    {
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<ItemSale> ItemSales { get; set; }
        public virtual DbSet<PlayerItem> PlayerItems { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public Task SaveChangesAsync() => base.SaveChangesAsync();
    }
}
