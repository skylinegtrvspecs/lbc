﻿using ItemShop.Data;
using ItemShop.Models;

namespace ItemShop.Repository
{
    public class ItemRepository : GenericRepository<Item>, IItemRepository
    {
        public ItemRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }
    }
}