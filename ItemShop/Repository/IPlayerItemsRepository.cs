﻿using ItemShop.Models;
using System;

namespace ItemShop.Repository
{
    public interface IPlayerItemRepository : IGenericRepository<PlayerItem>
    {
        PlayerItem Find(Guid playerId, Guid itemId);
    }
}
