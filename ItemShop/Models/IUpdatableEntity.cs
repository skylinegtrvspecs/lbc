﻿using System;

namespace ItemShop.Models
{
    public interface ICreateUpdateEntity
    {
        DateTime UpdatedDateTime { get; set; }
        DateTime CreatedDateTime { get; set; }
    }
}