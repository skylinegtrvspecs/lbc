﻿using System;
using System.Runtime.Serialization;

namespace ItemShop.Services
{
    [Serializable]
    internal class AlreadyOwnItemException : Exception
    {
        public AlreadyOwnItemException()
        {
        }

        public AlreadyOwnItemException(string message) : base(message)
        {
        }

        public AlreadyOwnItemException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AlreadyOwnItemException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}