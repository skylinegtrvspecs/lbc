﻿using ItemShop.Models;
using ItemShop.Repository;
using System;
using System.Threading.Tasks;

namespace ItemShop.Services
{
    public class ItemService : IItemService
    {
        private readonly IUnitOfWork unitOfWork;

        public ItemService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public ItemSale[] GetItemsForSale(int numberOfPlayersToSearch)
        {
            var playerIds = unitOfWork.ItemSales.GetRandom(numberOfPlayersToSearch);
            return unitOfWork.ItemSales.FindByPlayerIds(playerIds);
        }

        public async Task<ItemSale> PostItemForSale(Guid playerId, Guid itemId, decimal price)
        {
            var itemSale = unitOfWork.ItemSales.Find(playerId, itemId);

            if (itemSale != null)
                return itemSale;

            var playerItem = unitOfWork.PlayerItems.Find(playerId, itemId);
            if (playerItem == null || !playerItem.Active)
                throw new NoOwnageException();

            itemSale = new ItemSale
            {
                ItemSaleId = Guid.NewGuid(),
                ItemId = itemId,
                SellingPlayerId = playerId,
                Price = price,
                Active = true,
            };

            await unitOfWork.ItemSales.Add(itemSale);
            await unitOfWork.CompleteAsync();

            return itemSale;
        }

        public async Task<ItemSale> PurchaseItem(Guid playerId, Guid itemSalesId)
        {
            var itemSale = await unitOfWork.ItemSales.GetById(itemSalesId);

            if (itemSale == null || !itemSale.Active)
                throw new CannotCompletePurchase();

            var buyingPlayer = await unitOfWork.Players.GetById(playerId);

            if (buyingPlayer.Money < itemSale.Price)
                throw new InsufficientFundException();

            if (buyingPlayer.PlayerId == itemSale.SellingPlayerId)
                throw new AlreadyOwnItemException();

            itemSale.BuyingPlayerId = playerId;
            itemSale.Active = false;
            itemSale.SellingPlayer.Money += itemSale.Price;
            unitOfWork.ItemSales.Update(itemSale);
            buyingPlayer.Money -= itemSale.Price;
            unitOfWork.Players.Update(buyingPlayer);
            var playerItem = unitOfWork.PlayerItems.Find(itemSale.SellingPlayerId, itemSale.ItemId);
            playerItem.Active = false;
            unitOfWork.PlayerItems.Update(playerItem);

            await unitOfWork.PlayerItems.Add(new PlayerItem
            {
                PlayerItemId = Guid.NewGuid(),
                PlayerId = playerId,
                ItemId = itemSale.ItemId,
                Active = true,
            });

            await unitOfWork.CompleteAsync();

            return itemSale;
        }
    }
}