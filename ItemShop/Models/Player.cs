﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ItemShop.Models
{
    public class Player : ICreateUpdateEntity
    {
        [Key]
        public Guid PlayerId { get; set; }

        public string Name { get; set; }

        [EmailAddress]
        public string EmailAddress { get; set; }

        public decimal Money { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime UpdatedDateTime { get; set; }
    }
}
