﻿using ItemShop.Data;
using System;
using System.Threading.Tasks;

namespace ItemShop.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ApplicationDbContext applicationDbContext;

        public IItemRepository Items { get; }

        public IItemSaleRepository ItemSales { get; }

        public IPlayerRepository Players { get; }

        public IPlayerItemRepository PlayerItems { get; }

        public UnitOfWork(ApplicationDbContext applicationDbContext, IItemRepository itemRepository, IItemSaleRepository itemSaleRepository, IPlayerRepository playerRepository, IPlayerItemRepository playerItemRepository)
        {
            this.applicationDbContext = applicationDbContext;
            Items = itemRepository;
            ItemSales = itemSaleRepository;
            Players = playerRepository;
            PlayerItems = playerItemRepository;
        }

        public async Task CompleteAsync()
        {
            await applicationDbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            applicationDbContext.Dispose();
        }
    }
}
