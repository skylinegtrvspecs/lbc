﻿using ItemShop.Data;
using ItemShop.Models;
using System;
using System.Linq;

namespace ItemShop.Repository
{
    public class PlayerRepository : GenericRepository<Player>, IPlayerRepository
    {
        public PlayerRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }
    }
}
