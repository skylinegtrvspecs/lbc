﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItemShop.Models
{
    public class PlayerItem : ICreateUpdateEntity
    {
        [Key]
        public Guid PlayerItemId { get; set; }


        [ForeignKey("Player")]
        public Guid PlayerId { get; set; }

        public Player Player { get; set; }


        [ForeignKey("Item")]
        public Guid ItemId { get; set; }

        public Item Item { get; set; }

        public bool Active { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime UpdatedDateTime { get; set; }
    }
}
