using System;
using System.ComponentModel.DataAnnotations;

namespace ItemShop.Models
{
    public class Item : ICreateUpdateEntity
    {
        [Key]
        public Guid ItemId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime UpdatedDateTime { get; set; }
    }
}