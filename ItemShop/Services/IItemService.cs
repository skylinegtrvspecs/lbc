﻿using ItemShop.Models;
using System;
using System.Threading.Tasks;

namespace ItemShop.Services
{
    public interface IItemService
    {
        Task<ItemSale> PostItemForSale(Guid playerId, Guid itemId, decimal price);
        Task<ItemSale> PurchaseItem(Guid playerId, Guid itemSalesId);
        ItemSale[] GetItemsForSale(int numberOfPlayersToSearch);
    }
}