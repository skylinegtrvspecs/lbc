Use the following command to install Entity Framework tools:

dotnet tool install --global dotnet-ef

Use the following command to create the initial migration code:

dotnet ef migration add "Initial Migration"

Use the following command to update the database:

dotnet ef database update

Use the following insert statements to populate the database with test data:

insert into items (itemid, name, description, CreatedDateTime, UpdatedDateTime) values ('950F502C-551D-4485-9A32-73A533D402ED', 'item1', 'description1', datetime('now'), datetime('now'));
insert into items (itemid, name, description, CreatedDateTime, UpdatedDateTime) values ('950F502C-551D-4485-9A32-73A533D402EE', 'item2', 'description2', datetime('now'), datetime('now'));
insert into items (itemid, name, description, CreatedDateTime, UpdatedDateTime) values ('950F502C-551D-4485-9A32-73A533D402EF', 'item3', 'description3', datetime('now'), datetime('now'));

insert into Players (playerid, name, EmailAddress, Money, CreatedDateTime, UpdatedDateTime) values ('950F502C-551D-4485-9A32-73A533D402EA', 'player1', 'player1@player.com', 100, datetime('now'), datetime('now'));
insert into Players (playerid, name, EmailAddress, Money, CreatedDateTime, UpdatedDateTime) values ('950F502C-551D-4485-9A32-73A533D402EB', 'player2', 'player2@player.com', 100, datetime('now'), datetime('now'));
insert into Players (playerid, name, EmailAddress, Money, CreatedDateTime, UpdatedDateTime) values ('950F502C-551D-4485-9A32-73A533D402EC', 'player3', 'player3@player.com', 100, datetime('now'), datetime('now'));

insert into PlayerItems (PlayerItemId, playerid, itemid, active, CreatedDateTime, UpdatedDateTime) values ('851F502C-551D-4485-9A32-73A533D402EF', '950F502C-551D-4485-9A32-73A533D402EA', '950F502C-551D-4485-9A32-73A533D402ED', true, datetime('now'), datetime('now'));
insert into PlayerItems (PlayerItemId, playerid, itemid, active, CreatedDateTime, UpdatedDateTime) values ('852F502C-551D-4485-9A32-73A533D402EF', '950F502C-551D-4485-9A32-73A533D402EB', '950F502C-551D-4485-9A32-73A533D402EE', true, datetime('now'), datetime('now'));
insert into PlayerItems (PlayerItemId, playerid, itemid, active, CreatedDateTime, UpdatedDateTime) values ('853F502C-551D-4485-9A32-73A533D402EF', '950F502C-551D-4485-9A32-73A533D402EC', '950F502C-551D-4485-9A32-73A533D402EF', true, datetime('now'), datetime('now'));

After building and running the project, use the following to test the different functionalities:
https://localhost:44366/item/itemforsale?playerId=950F502C-551D-4485-9A32-73A533D402EA&itemId=950F502C-551D-4485-9A32-73A533D402ED&price=50
https://localhost:44366/item/itemforsale?numberOfPlayersToSearch=1
https://localhost:44366/item/ItemPurchase?playerId=950F502C-551D-4485-9A32-73A533D402EC&itemSalesId={generated from the first API call}